'use strict';
var Sequelize = require('sequelize');
var path = require('path');
var co = require('co');

var passutil = require('./passutil.js');
var config = require('../config.js');

var db = new Sequelize(config.db, {
    logging: config.environment === 'production' ? false : true
});

db.import(path.join(__dirname, 'db-def.js'));
var syncPromise = db.sync().then(co.wrap(function* () {
    // SELECT count(*) AS "count" FROM "users";
    var userNum = yield db.models.user.count();
    if (userNum === 0) {
        // init superuser
        var passwords = yield passutil.createPassword(config.initPassword);
        // INSERT INTO "users" ("id","username","password","salt","superuser","createdAt","updatedAt") VALUES (DEFAULT,'admin','J018a0iOQDlR7s5NN3e103dIu5GiTOH52Bv6F07G2SQ=','xNuwrJKj/RxvAehVSD7e7w==',true,'2016-04-21 13:31:36.992 +00:00','2016-04-21 13:31:36.992 +00:00');
        yield db.models.user.create({
            username: config.initUser,
            password: passwords.password,
            salt: passwords.salt,
            superuser: true
        });
    }
}));
db.onSync = syncPromise.then.bind(syncPromise);
module.exports = db;
