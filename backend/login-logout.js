'use strict';
var db = require('./db.js');
var passutil = require('./passutil.js');
module.exports = function* (next) {
    if (this.path === '/logout') {
        this.session = null;
        this.redirect('/login');
    } else if (this.path === '/login' && this.request.method === 'POST') {
        var postdata = yield* this.request.urlencoded();
        // SELECT "id", "username", "password", "salt" FROM "users" WHERE "username" = 'admin' LIMIT 1;
        var userObject = yield db.models.user.findOne({
            where: { username: postdata.username },
            attributes: ['id', 'username', 'password', 'salt']
        });
        if (userObject !== null) {
            if (yield passutil.checkPassword(postdata.password, userObject.password, userObject.salt)) {
                this.session.userId = userObject.id;
                if (this.session.loginRedirect) this.redirect(this.session.loginRedirect);
                else this.redirect('/');
                this.session.loginRedirect = null;
                return;
            }
        }
        this.redirect('/login#failed');
    } else if ((!this.session || !this.session.userId) && this.path !== '/login') {
        this.session = { loginRedirect: this.path };
        this.redirect('/login');
    } else {
        yield next;
    }
};
