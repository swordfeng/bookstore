var crypto = require('crypto');
var Promise = require('bluebird');
var co = require('co');

var createPassword = module.exports.createPassword = co.wrap(function* (plainPassword, salt) {
    var salt = salt || crypto.randomBytes(16);
    var password = yield (Promise.promisify(crypto.pbkdf2)(plainPassword, salt, 100000, 32, 'sha512'));
    return {
        salt: salt.toString('base64'),
        password: password.toString('base64')
    };
});

module.exports.checkPassword = co.wrap(function* (plainPassword, password, salt) {
    var calculated = yield createPassword(plainPassword, Buffer(salt, 'base64'));
    return calculated.password === password; // maybe not safe
});
