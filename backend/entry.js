'use strict';
var config = require('../config.js');
process.env.NODE_ENV = config.environment;
var fs = require('fs');
var path = require('path');
var koala = require('koala');
var mount = require('koa-mount');

var db = require('./db');
var passutil = require('./passutil');

var app = koala();

require('koa-spdy')(app);

app.keys = ['secret key YxFjFEm+dBcXCFZanZA04Jdn2aB7dniiN/7Y98xqdXA='];

app
.use(mount('/favicon.ico', function* () {
    yield* this.fileServer.send('favicon.ico');
}))
.use(mount('/static', function* () {
    yield* this.fileServer.send();
}))
.use(mount('/api/v1', require('./rest-api')))
.use(require('./login-logout.js'))
.use(function* () {
    yield this.fileServer.send('index.html');
    if (this.isSpdy) {
        yield this.fileServer.push('static/bundle.js');
        yield this.fileServer.push('favicon.ico');
    }
});

var keys = config.keys;
var cb = app.callback();
var server = require('spdy').createServer(keys, cb);

db.onSync(() => server.listen(config.port, () => console.log('server listening')));
