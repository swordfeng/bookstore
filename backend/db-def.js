module.exports = function (sequelize, DataTypes) {
    var user = sequelize.define('user', {
        username: {
            type: DataTypes.STRING(32),
            allowNull: false,
            unique: true
        },
        password: DataTypes.STRING(44),
        salt: DataTypes.STRING(24),
        superuser: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        realName: DataTypes.STRING,
        workNumber: {
            type: DataTypes.STRING,
            unique: true
        },
        gender: DataTypes.STRING,
        age: DataTypes.INTEGER
    });
    var book = sequelize.define('book', {
        isbn: {
            type: DataTypes.STRING(13),
            unique: true,
            validate: {
                is: /^[0-9]{13,13}$/,
                /*
                isbn: function (value) {
                    if (!ISBN.isIsbn13(value)) {
                        throw new Error('Invalid ISBN');
                    }
                }
                */
            }
        },
        title: DataTypes.STRING,
        author: DataTypes.STRING,
        press: DataTypes.STRING(20),
        price: {
            type: DataTypes.DECIMAL(10,2),
            validate: {
                min: 0
            }
        },
        store: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
            validate: {
                min: 0
            }
        }
    });
    var purchase = sequelize.define('purchase', {
        price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            validate: {
                min: 0
            }
        },
        number: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 1
            }
        },
        status: {
            type: DataTypes.ENUM('unpaid', 'paid', 'stored', 'canceled'),
            allowNull: false,
            defaultValue: 'unpaid'
        }
    });
    var bill = sequelize.define('bill', {
        type: DataTypes.STRING,
        value: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        description: DataTypes.STRING
    });
    purchase.belongsTo(book);
}
