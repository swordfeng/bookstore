'use strict';
var koa = require('koa');
var mount = require('koa-mount');
var restapp = koa();

var _ = require('lodash');
var util = require('util');
var co = require('co');

var db = require('./db');
var passutil = require('./passutil');


function CustomError(message, statusCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.statusCode = statusCode;
};
util.inherits(CustomError, Error);

restapp
.use(function* (next) {
    // error handler
    try {
        yield next;
    } catch (e) {
        if (e instanceof db.Error || e instanceof CustomError) {
            this.status = e.statusCode || 400;
            this.body = { type: 'error', message: e.message };
            return;
        }
        throw e;
    }
})
.use(function* (next) {
    // check login
    if (typeof this.session.userId === 'number') {
        // SELECT * FROM "users" WHERE "id" = 1;
        this.state.userObj = yield db.models.user.findById(this.session.userId);
    }
    if (!this.state.userObj) {
        this.session = null;
        throw new CustomError('need login', 403);
    }
    yield next;
})
.use(mount('/reset', function* () {
    if (!this.state.userObj.superuser) {
        throw new CustomError('operation not permitted', 403);
    }
    this.body = { type: 'success', message: 'Success' };
}))
.use(mount('/currentUser', function* () {
    this.body = {
        id: this.state.userObj.id,
        username: this.state.userObj.username,
        superuser: this.state.userObj.superuser
    };
}))
// universal query format
.use(function* (next) {
    if (this.request.method === 'GET') {
        this.state.query = this.query;
    } else {
        this.state.query = yield this.request.urlencoded();
    }
    yield next;
})
.use(mount('/user', function* () {
    var obj = _.chain(this.state.query).omit([ 'superuser', 'password', 'salt' ]).pickBy(v => v).value();
    if (!this.state.userObj.superuser && obj.id !== this.session.userId) {
        throw new CustomError('operation not permitted', 403);
    }
    if (this.request.method === 'GET') {
        // SELECT "id", "username", "superuser", "realName", "workNumber", "gender", "age" FROM "users";
        this.body = (yield db.models.user.findAll({
            where: obj,
            attributes: [ 'id', 'username', 'superuser', 'realName', 'workNumber', 'gender', 'age' ]
        })).map(x => x.dataValues);
    } else if (this.request.method === 'POST') {
        if (this.state.userObj.superuser) {
            obj.superuser = this.state.query.superuser;
        }
        if (this.state.query.password) {
            var pass = yield passutil.createPassword(this.state.query.password);
            obj.password = pass.password;
            obj.salt = pass.salt;
        }
        if (!obj.id) {
            // INSERT INTO "users" ("id","username","password","salt","superuser","realName","workNumber","gender","age","createdAt","updatedAt") VALUES (DEFAULT,'aa','2MqrCq/yK6qlD1vxlJR65NdPuRcY8qVmwQ5o7/ueHvI=','suiHEsDQ5Uo0RxmWXbRg1A==',false,'cc','dd','ee','12','2016-04-20 06:31:57.884 +00:00','2016-04-20 06:31:57.884 +00:00');
            yield db.models.user.create(obj);
        } else {
            // UPDATE "users" SET "id"='2',"username"='test',"realName"='测试用户',"workNumber"='NO.2',"gender"='女',"age"='19',"superuser"=false,"updatedAt"='2016-04-20 06:32:33.255 +00:00' WHERE "id" = '2';
            yield db.models.user.update(obj, { where: { id: obj.id } });
        }
        this.body = { type: 'success', message: 'Success' };
    } else if (this.request.method === 'DELETE') {
        // DELETE FROM "users" WHERE "id" = '3';
        yield db.models.user.destroy({ where: { id: obj.id } });
        this.body = { type: 'success', message: 'Success' };
    } else throw new CustomError('unknown operation', 403);
}))
.use(mount('/book', function* () {
    var obj = this.state.query;
    if (this.request.method === 'GET') {
        var where = {};
        if (obj.id) where.id = obj.id;
        if (obj.isbn) where.isbn = { $like: obj.isbn + '%' };
        if (obj.title) where.title = { $like: '%' + obj.title + '%' };
        if (obj.author) where.author = { $like: '%' + obj.author + '%' };
        if (obj.press) where.press = { $like: '%' + obj.press + '%' };
        if (obj.store === 'true') where.store = { $gt: 0 };
        // SELECT * from "books" where "id"='1' and "isbn" LIKE '0123456789123%' and "title" LIKE '%asd%' and  "author" LIKE '%aaa%' and "press" LIKE 'bbb' and "store" > 0;
        // 以上条件可选
        this.body = (yield db.models.book.findAll({ where: where })).map(x => x.dataValues);
    } else if (this.request.method === 'POST') {
        if (obj.id) {
            // UPDATE "books" SET "id"='1',"title"='asdfasdfasd',"isbn"='0123456789123',"author"='aaa',"press"='bbb',"price"='1',"updatedAt"='2016-04-21 10:11:45.771 +00:00' WHERE "id" = '1';
            yield db.models.book.update(obj, { where: { id: obj.id } });
        } else {
            // INSERT INTO "books" ("id","isbn","title","author","press","price","store","createdAt","updatedAt") VALUES (DEFAULT,'9787111464778','计算机系统基础','袁春风','机械工业出版社','49',0,'2016-04-21 10:13:34.489 +00:00','2016-04-21 10:13:34.489 +00:00');
            yield db.models.book.create(obj);
        }
        this.body = { type: 'success', message: 'Success' };
    } else if (this.request.method === 'DELETE') {
        // DELETE FROM "books" WHERE "id" = '1';
        yield db.models.book.destroy({ where: { id: obj.id } });
        this.body = { type: 'success', message: 'Success' };
    } else throw new CustomError('unknown operation', 403);
}))
.use(mount('/purchase', function* () {
    if (this.request.method === 'GET') {
        // SELECT * FROM "purchases" LEFT OUTER JOIN "books" ON "purchases"."bookId" = "books"."id";
        this.body = yield db.models.purchase.findAll({
            include: [ db.models.book ]
        });
    } else if (this.request.method === 'POST') {
        var obj = this.state.query;
        yield db.transaction(co.wrap(function* (t) {
            var p;
            switch (obj.operation) {
                case 'purchase':
                    // INSERT INTO "purchases" ("id","price","number","status","createdAt","updatedAt","bookId") VALUES (DEFAULT,'30','100','unpaid','2016-04-21 10:23:49.622 +00:00','2016-04-21 10:23:49.622 +00:00','2');
                    yield db.models.purchase.create(_.pick(obj, ['number', 'price', 'bookId']));
                    break;
                case 'pay':
                    // SELECT * FROM "purchases" LEFT OUTER JOIN "books" ON "purchases"."bookId" = "books"."id" WHERE "purchases"."id" = '1';
                    p = yield db.models.purchase.findOne({ where: { id: obj.id }, include: [ db.models.book ], transaction: t });
                    if (!p.book) throw new CustomError('invalid book!', 400);
                    if (p.status !== 'unpaid') throw new CustomError('not an unpaid purchase', 400);
                    // INSERT INTO "bills" ("id","type","value","description","createdAt","updatedAt") VALUES (DEFAULT,'purchase',-3000,'进货《计算机系统基础》（isbn = 9787111464778）100本','2016-04-21 10:24:47.214 +00:00','2016-04-21 10:24:47.214 +00:00');
                    yield db.models.bill.create({
                        type: 'purchase',
                        value: - p.number * p.price,
                        description: '进货《' + p.book.title + '》（isbn = ' + p.book.isbn + '）' + p.number + '本'
                    }, { transaction: t });
                    // UPDATE "purchases" SET "status"='paid',"updatedAt"='2016-04-21 10:24:47.220 +00:00' WHERE "id" = '1';
                    p.status = 'paid';
                    yield p.save({transaction: t});
                    break;
                case 'store':
                    // SELECT * FROM "purchases" LEFT OUTER JOIN "books" ON "purchases"."bookId" = "books"."id" WHERE "purchases"."id" = '1';
                    p = yield db.models.purchase.findOne({ where: { id: obj.id }, include: [ db.models.book ], transaction: t });
                    if (!p.book) throw new CustomError('invalid book!', 400);
                    if (p.status !== 'paid') throw new CustomError('not a paid purchase', 400);
                    // UPDATE "books" SET "store"=100,"updatedAt"='2016-04-21 10:26:55.155 +00:00' WHERE "id" = 2;
                    p.book.store += p.number;
                    yield p.book.save({transaction: t});
                    // UPDATE "purchases" SET "status"='stored',"updatedAt"='2016-04-21 10:26:55.159 +00:00' WHERE "id" = '1';
                    p.status = 'stored';
                    yield p.save({transaction: t});
                    break;
                case 'cancel':
                    // SELECT * FROM "purchases" LEFT OUTER JOIN "books" ON "purchases"."bookId" = "books"."id" WHERE "purchases"."id" = '6';
                    p = yield db.models.purchase.findOne({ where: { id: obj.id }, include: [ db.models.book ], transaction: t });
                    if (!p.book) throw new CustomError('invalid book!', 400);
                    if (p.status !== 'unpaid') throw new CustomError('not an unpaid purchase', 400);
                    // UPDATE "purchases" SET "status"='canceled',"updatedAt"='2016-04-21 12:55:59.761 +00:00' WHERE "id" = '6'
                    p.status = 'canceled';
                    p.save({transaction: t});
                    break;
                default:
                    throw new CustomError('operation undefined', 400);
            }
        }).bind(this));
        this.body = { type: 'success', message: 'Success' };
    } else throw new CustomError('unknown operation', 403);
}))
.use(mount('/sale', function* () {
    if (this.request.method === 'POST') {
        var obj = this.state.query;
        obj.number = parseInt(obj.number);
        if (!obj.number || obj.number < 0) throw new CustomError('非法数量', 400);
        yield db.transaction(co.wrap(function* (t) {
            // SELECT * FROM "books" WHERE "id" = '2';
            var book = yield db.models.book.findOne({ where: { id: obj.bookId }, transaction: t });
            if (book.store < obj.number) throw new CustomError('库存不足', 400);
            // UPDATE "books" SET "store"=1120,"updatedAt"='2016-04-21 13:17:03.963 +00:00' WHERE "id" = 2;
            book.store -= obj.number;
            yield book.save({ transaction: t });
            // INSERT INTO "bills" ("id","type","value","description","createdAt","updatedAt") VALUES (DEFAULT,'sale',147,'销售《计算机系统基础》（isbn = 9787111464778）3本','2016-04-21 13:17:03.973 +00:00','2016-04-21 13:17:03.973 +00:00');
            yield db.models.bill.create({
                type: 'sale',
                value: obj.number * book.price,
                description: '销售《' + book.title + '》（isbn = ' + book.isbn + '）' + obj.number + '本'
            }, { transaction: t });
        }).bind(this));
        this.body = { type: 'success', message: 'Success' };
    } else throw new CustomError('unknown operation', 403);
}))
.use(mount('/bill', function* () {
    // SELECT * FROM "bills" WHERE ("createdAt" >= '2016-01-01 00:00:00.000 +00:00' AND "createdAt" <= '2016-04-21 13:19:55.703 +00:00') ORDER BY "createdAt" DESC;
    this.body = (yield db.models.bill.findAll({
        where: {
            createdAt: {
                $gte: parseInt(this.state.query.from),
                $lte: parseInt(this.state.query.to)
            }
        },
        order: [[ 'createdAt', 'DESC' ]]
    })).map(o => o.dataValues);
}))
.use(function* () {
    throw new CustomError('unknown operation', 403);
});

module.exports = restapp;
