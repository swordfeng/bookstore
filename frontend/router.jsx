'use strict';
var NavBar = require('./component/navbar.jsx');
var MessageBanner = require('./component/banner.jsx');
var Router = module.exports = React.createClass({
    route: function () {
        var querystring = window.location.search.substring(1);
        this.setState({
            path: window.location.pathname,
            search: window.location.search,
            hash: window.location.hash
        });
        eventCenter.emit('clearMessage');
    },
    getInitialState: function () {
        return {
            path: null,
            search: null,
            hash: null
        };
    },
    componentDidMount: function () {
        this.route();
        eventCenter.on('route', this.route);
    },
    componentWillUnmount: function () {
        eventCenter.removeListener('route', this.route);
    },
    render: function () {
        if (!this.props.routes) return null;
        for (var item of this.props.routes) {
            var routeMatch = item[0].exec(this.state.path);
            if (routeMatch !== null) {
                document.title = item[2] + ' - 图书管理系统';
                var content = React.createElement(item[1], {
                    routeMatch: routeMatch
                });
                return (
                    <div>
                        <NavBar userinfo={this.state.userinfo} />
                        <div className="container"><MessageBanner /></div>
                        {content}
                    </div>
                );
            }
        }
        document.title = '404 - 图书管理系统';
        return this.props.notFound || null;
    }
});
