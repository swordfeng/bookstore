'use strict';
var co = require('co');
var ISBNInput = require('./component/isbninput.jsx');
var _ = require('lodash');
module.exports = React.createClass({
    getInitialState: function () {
        return {
            newBook: false,
            bookId: parseInt(this.props.routeMatch[1]) || false,
            bookInfo: {
                isbn: '',
                title: '',
                author: '',
                press: '',
                price: ''
            },
        };
    },
    componentDidMount: function () {
        if (this.state.bookId) {
            $.ajax('/api/v1/book?id=' + this.state.bookId)
                .then(v => {
                    this.setState({ bookInfo: v[0] });
                });
        }
    },
    submit: function (e) {
        e.preventDefault();
        co.wrap(function* () {
            var bookId;
            if (this.state.newBook) {
                // submit new book first
                yield $.ajax('/api/v1/book', {
                    method: 'POST',
                    data: this.state.bookInfo
                });
                bookId = (yield $.ajax('/api/v1/book?isbn=' + this.state.bookInfo.isbn))[0].id;
                this.setState({ newBook: false });
            } else bookId = this.state.bookInfo.id;
            if (!bookId) return { type: 'error', message: 'invalid isbn' };
            return yield $.ajax('/api/v1/purchase', {
                method: 'POST',
                data: {
                    operation: 'purchase',
                    bookId: bookId,
                    number: this.refs.form.number.value,
                    price: this.refs.form.price.value
                }
            });
        }).bind(this)()
        .then(m => eventCenter.emit('message', m))
        .catch(e => { if (e instanceof Error) console.error(e.stack); else throw e; })
        .catch((xhr) => eventCenter.emit('message', xhr.responseJSON));
        return false;
    },
    isbnChange: function (e) {
        var isbn = e.target.value;
        this.setState({ newBook: false, bookInfo: { isbn: isbn } });
        if (e.target.value.length !== 13) return;
        $.ajax('/api/v1/book', {
            data: { isbn: isbn },
            success: (res) => {
                if (this.state.bookInfo.isbn !== isbn) return;
                if (res.length > 0) {
                    this.setState({ bookInfo: res[0] });
                } else {
                    this.setState({ newBook: true });
                }
            }
        });
    },
    setBookInfo: function (index) {
        return (e) => {
            var v = e.target.value;
            if (index === 'price') {
                v = /[0-9]*\.{0,1}[0-9]{0,2}/.exec(v)[0];
            }
            this.setState(function (p) {
                p.bookInfo[index] = v;
                return p;
            })
        };
    },
    isbnLink: function (json) {
        if (!this.state.newBook) return;
        this.setState(function (state) {
            if (json.title) state.bookInfo.title = json.title;
            if (json.author) state.bookInfo.author = json.author.join(', ');
            if (json.publisher) state.bookInfo.press = json.publisher;
            if (json.price) state.bookInfo.price = /[0-9\.]+/.exec(json.price)[0];
        });
    },
    validatePrice: function () {
        var val = this.refs.form.price.value;
        val = /[0-9]*\.{0,1}[0-9]{0,2}/.exec(val)[0];
        this.refs.form.price.value = val;
    },
    render: function () {
        var bookInfo = this.state.bookInfo;
        return (
            <form onSubmit={this.submit} className="form container" ref="form">
                <label>ISBN:</label>
                <ISBNInput value={bookInfo.isbn} onChange={this.isbnChange} isbnlink={this.isbnLink} />
                <label>标题:</label>
                <input type="text" className="form-control" name="bookTitle" disabled={!this.state.newBook} value={bookInfo.title} onChange={this.setBookInfo('title')} />
                <label>作者:</label>
                <input type="text" className="form-control" name="bookAuthor" disabled={!this.state.newBook} value={bookInfo.author} onChange={this.setBookInfo('author')} />
                <label>出版社:</label>
                <input type="text" className="form-control" name="bookPress" disabled={!this.state.newBook} value={bookInfo.press} onChange={this.setBookInfo('press')} />
                <label>单价:</label>
                <input type="text" className="form-control" name="bookPrice" disabled={!this.state.newBook} value={bookInfo.price} onChange={this.setBookInfo('price')} />
                <label>进货数量:</label>
                <input type="number" className="form-control" name="number" />
                <label>进货单价:</label>
                <input type="text" className="form-control" name="price" onChange={this.validatePrice} />
                <input type="submit" className="btn" value="进货" />
            </form>
        );
    }
});
