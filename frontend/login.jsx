'use strict';
module.exports = React.createClass({
    render: function () {
        var failmsg = null;
        if (window.location.hash === '#failed') {
            failmsg = (<div className="alert alert-danger">Login failed.</div>);
        }
        return (
            <div className="container">
                <div className="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 jumbotron">
                    <form action="/login" method="post">
                        <h1>Login</h1>
                        {failmsg}
                        <label>Username:</label><br />
                        <input type="text" className="form-control" name="username" /><br />
                        <label>Password:</label><br />
                        <input type="password" className="form-control" name="password" /><br />
                        <input type="submit" className="btn" /><br />
                    </form>
                </div>
            </div>
        );
    }
});
