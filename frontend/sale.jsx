'use strict';
var co = require('co');
var ISBNInput = require('./component/isbninput.jsx');
module.exports = React.createClass({
    getInitialState: function () {
        return {
            bookId: parseInt(this.props.routeMatch[1]) || false,
            bookInfo: {
                id: '',
                isbn: '',
                title: '',
                author: '',
                press: '',
                price: '',
                store: '',
            },
            number: 1
        };
    },
    componentDidMount: function () {
        if (this.state.bookId) {
            $.ajax('/api/v1/book?id=' + this.state.bookId)
                .then(v => {
                    this.setState({ bookInfo: v[0] });
                });
        }
    },
    submit: function (e) {
        e.preventDefault();
        var bookId = this.state.bookInfo.id;
        if (!bookId) eventCenter.emit('message', { type: 'error', message: 'invalid book' });
        else {
            $.ajax('/api/v1/sale', {
                method: 'POST',
                data: {
                    bookId: bookId,
                    number: this.state.number,
                },
                complete: xhr => {
                    eventCenter.emit('message', xhr.responseJSON);
                    $.ajax('/api/v1/book?id=' + this.state.bookInfo.id)
                        .then(v => {
                            this.setState({ bookInfo: v[0] });
                        });
                }
            });
        }
        return false;
    },
    changeIsbn: function (e) {
        var isbn = e.target.value;
        this.setState({ newBook: false, bookInfo: { isbn: isbn } });
        if (e.target.value.length !== 13) return;
        $.ajax('/api/v1/book', {
            data: { isbn: isbn },
            success: (res) => {
                if (this.state.bookInfo.isbn !== isbn) return;
                if (res.length > 0) {
                    this.setState({ bookInfo: res[0] });
                }
            }
        });
    },
    changeNumber: function (e) {
        this.setState({ number: e.target.value });
    },
    render: function () {
        var bookInfo = this.state.bookInfo;
        return (
            <form onSubmit={this.submit} className="form container" ref="form">
                <label>ISBN:</label>
                <ISBNInput value={bookInfo.isbn} onChange={this.changeIsbn} />
                <label>BookID:</label>
                <input type="text" className="form-control" disabled value={bookInfo.id} />
                <label>标题:</label>
                <input type="text" className="form-control" disabled value={bookInfo.title} />
                <label>作者:</label>
                <input type="text" className="form-control" disabled value={bookInfo.author} />
                <label>出版社:</label>
                <input type="text" className="form-control" disabled value={bookInfo.press} />
                <label>单价:</label>
                <input type="text" className="form-control" disabled value={bookInfo.price} />
                <label>库存数量:</label>
                <input type="text" className="form-control" disabled value={bookInfo.store} />
                <label>出售量:</label>
                <input type="text" className="form-control" value={this.state.number} onChange={this.changeNumber} />
                <label>总价:</label>
                <input type="text" className="form-control" disabled value={(bookInfo.price*this.state.number).toFixed(2)} />
                <input type="submit" className="btn" value="出售" />
            </form>
        );
    }
});
