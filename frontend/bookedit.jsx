'use strict';
var DataForm = require('./component/dataform.jsx');
module.exports = React.createClass({
    getInitialState: function () {
        var schema = {
            id: {
                type: 'text',
                label: 'BookID',
                disabled: true
            },
            title: {
                type: 'text',
                label: '标题',
                isbnlink: function (json) {
                    return json.title;
                }
            },
            isbn: {
                type: 'isbn',
                label: 'ISBN'
            },
            author: {
                type: 'text',
                label: '作者',
                isbnlink: function (json) {
                    return json.author.join(', ');
                }
            },
            press: {
                type: 'text',
                label: '出版社',
                isbnlink: function (json) {
                    return json.publisher;
                }
            },
            price: {
                type: 'text',
                label: '单价',
                validate: /^[0-9]*\.{0,1}[0-9]{0,2}$/,
                isbnlink: function (json) {
                    var match = /[0-9\.]+/.exec(json.price);
                    if (match) {
                        return match[0];
                    }
                    return '';
                }
            }
        };
        var id = parseInt(this.props.routeMatch[1]) || null;
        return {
            schema: schema,
            id: id,
            info: null
        };
    },
    componentDidMount: function () {
        if (this.state.id) {
            $.ajax('/api/v1/book?id=' + this.state.id, {
                complete: (xhr) => {
                    var info = xhr.responseJSON;
                    if (info.length > 0) {
                        this.setState({ info: info[0] });
                    }
                }
            });
        }
    },
    submit: function (data) {
        data.id = this.state.id;
        $.ajax('/api/v1/book', {
            method: 'POST',
            data: data,
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
            }
        });
    },
    delete: function (id) {
        $.ajax('/api/v1/book', {
            method: 'DELETE',
            data: { id: id },
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
            }
        });
    },
    render: function () {
        var info = { id: '<new>' };
        if (this.state.id) {
            if (!this.state.info) return null;
            info = this.state.info;
        }
        return (
            <div className="container">
                <DataForm schema={this.state.schema} data={info} onSubmit={this.submit} onDelete={this.delete} message={this.state.message} />
            </div>
        );
    }
});
