'use strict';
var ISBNInput = require('./isbninput.jsx');
var count = 0;
module.exports = React.createClass({
    getInitialState: function () {
        var state = { data: {} };
        for (var name in this.props.schema) {
            state.data[name] = this.props.data[name] || (this.props.schema[name].type === 'checkbox' ? false : '');
        }
        return state;
    },
    submit: function (e) {
        e.preventDefault();
        this.props.onSubmit(this.state.data);
        return false;
    },
    delete: function () {
        this.props.onDelete(this.props.data.id);
    },
    change: function (name) {
        return (e) => {
            var value;
            if (e.target.type === 'checkbox') value = e.target.checked;
            else value = e.target.value;
            if (this.props.schema[name].validate) {
                if (!this.props.schema[name].validate.test(value)) {
                    return;
                }
            }
            this.setState(function (state) {
                state.data[name] = value;
                return state;
            });
        };
    },
    isbnLink: function (json) {
        for (var name in this.props.schema) {
            var link = this.props.schema[name].isbnlink;
            if (link) {
                this.setState(function (state) {
                    state.data[name] = link(json);
                });
            }
        }
    },
    render: function () {
        var schema = this.props.schema;
        var message = this.props.message || null;
        var data = this.state.data;
        var formitems = [];
        for (var name in schema) {
            var schemaitem = schema[name];
            if (schemaitem.type === 'text' || schemaitem.type === 'password') {
                formitems.push(
                    <div className="form-group" key={name}>
                        <label>{schemaitem.label}:</label>
                        <input
                            className="form-control"
                            type={schemaitem.type}
                            disabled={schemaitem.disabled || false}
                            value={data[name]}
                            onChange={this.change(name)}
                            name={name}
                            placeholder={schemaitem.placeholder || ''}
                        />
                    </div>
                );
            } else if (schemaitem.type === 'checkbox') {
                formitems.push(
                    <div className="form-group" key={name}>
                        <label>{schemaitem.label}:</label>
                        <input
                            type={schemaitem.type}
                            disabled={schemaitem.disabled || false}
                            checked={data[name]}
                            onChange={this.change(name)}
                            name={name}
                        />
                    </div>
                );
            } else if (schemaitem.type === 'isbn') {
                formitems.push(
                    <div className="form-group" key={name}>
                        <label>{schemaitem.label}:</label>
                        <ISBNInput
                            className="form-control"
                            disabled={schemaitem.disabled || false}
                            onChange={this.change(name)}
                            name={name}
                            isbnlink={this.isbnLink}
                        />
                    </div>
                );
            }
        }
        return (
            <form onSubmit={this.submit} className="form" ref="form">
                {formitems}
                <input type="submit" className="btn" defaultValue={data.id !== '<new>' ? "更新" : "创建"} />
                {
                    data.id === '<new>' ? null : (
                        <input type="button" className="btn" defaultValue="删除" onClick={this.delete} />
                    )
                }
            </form>
        );
    }
});
