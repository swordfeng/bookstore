'use strict';
module.exports = React.createClass({
    render: function () {
        var head = null;
        if (this.props.head) {
            var headlist = [];
            for (var item of this.props.head) {
                headlist.push(<th>{item}</th>);
            }
            head = (
                <thead>
                    <tr>
                        {headlist}
                    </tr>
                </thead>
            );
        }
        var body = this.props.body.map((row) => (
            <tr>
                {row.map((item) => (<td>{item}</td>))}
            </tr>
        ));
        return (
            <table className="table table-striped">
                {head}
                <tbody>{body}</tbody>
            </table>
        )
    }
});
