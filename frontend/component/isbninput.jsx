'use strict';
var ISBNScanner = require('./isbnscanner.jsx');
module.exports = React.createClass({
    getInitialState: function () {
        return { isbn: this.props.value || '', scannerOn: false };
    },
    componentWillReceiveProps: function (nextProps) {
        if (nextProps.value) this.setState({ isbn: nextProps.value });
    },
    onChange: function (e) {
        if (!/^[0-9]*$/.test(e.target.value)) return;
        this.setState({ isbn: e.target.value });
        if (this.props.onChange) this.props.onChange(e);
    },
    onDetected: function (code) {
        this.setState({ isbn: code });
        this.setState({ scannerOn: false });
        if (this.props.onChange) this.props.onChange({target: this.refs.input});
    },
    toggleScanner: function () {
        this.setState(function (state) { state.scannerOn = ! state.scannerOn; return state; });
    },
    searchDouban: function () {
        if (this.state.isbn.length !== 13) return;
        var isbnlink = this.props.isbnlink;
        if (!isbnlink) return;
        $.ajax({
            url: "https://api.douban.com/v2/book/isbn/:" + this.state.isbn,
            jsonp: "callback",
            dataType: "jsonp",
            success: (response) => {
                isbnlink(response);
            }
        });
    },
    render: function () {
        var scanner = null;
        if (this.state.scannerOn) {
            scanner = [(<br />), (<ISBNScanner onDetected={this.onDetected}/>)];
        }
        var douban = null;
        if (this.props.isbnlink) {
            douban = (
                <span className="input-group-btn">
                    <button className="btn btn-default" type="button" onClick={this.searchDouban}>豆</button>
                </span>
            );
        }
        return (<div>
            <div className="input-group">
                <input type="text" name={this.props.name} className="form-control" ref="input" value={this.state.isbn} onChange={this.onChange} maxlength="13" />
                <span className="input-group-btn">
                    <button className="btn btn-default glyphicon glyphicon-camera" type="button" ref="camera" onClick={this.toggleScanner}></button>
                </span>
                {douban}
            </div>
            {scanner}
        </div>);
    }
});
