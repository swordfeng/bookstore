'use strict';
var Quagga = require('quagga');
var detected = null;
Quagga.onDetected(function (data) {
    if (detected) detected(data);
});
module.exports = React.createClass({
    componentDidMount: function () {
        Quagga.init({
            "inputStream":{
                "type":"LiveStream",
                target: this.refs.image
            },
            "numOfWorkers":4,
            "decoder":{
                "readers":["ean_reader"]
            }
        }, (err) => {
            if (err) {
                console.error(err.stack);
                return;
            }
            Quagga.start();
            detected = this.onDetected;
            $(this.refs.image).find('video').css('width', '100%');
            $(this.refs.image).find('canvas').css('display', 'none');
        });
    },
    componentWillUnmount: function () {
        Quagga.stop();
        detected = null;
    },
    onDetected: function (data) {
        if (this.props.onDetected) this.props.onDetected(data.codeResult.code);
    },
    render: function () {
        return (<div ref="image"/>);
    }
});
