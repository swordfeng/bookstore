'use strict';
const navinfo = [
    // [href, path-test, display-name, superuser]
    ['/', /^\/$/, '主页'],
    ['/user', /^\/user/, '用户管理', true],
    ['/book', /^\/book/, '书籍管理', false],
    ['/purchase', /^\/purchase/, '进货管理', false],
    ['/sale', /^\/sale/, '图书销售', false],
    ['/financial', /^\/financial/, '财务管理', false]
];
module.exports = React.createClass({
    setUserInfo: function (userinfo) {
        this.setState({ userinfo: userinfo });
    },
    getInitialState: function () {
        return {
            userinfo: globalState.userinfo
        };
    },
    componentDidMount: function () {
        eventCenter.on('userinfo', this.setUserInfo);
    },
    componentWillUnmount: function () {
        eventCenter.removeListener('userinfo', this.setUserInfo);
    },
    render: function () {
        if (!this.state.userinfo) return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="/">图书管理系统</a>
                    </div>
                </div>
            </nav>
        );
        console.log('userinfo', this.state.userinfo);
        var list = [];
        for (var i in navinfo) {
            var info = navinfo[i];
            var item = (
                <li className={info[1].test(window.location.pathname) ? 'active' : ''} key={i}>
                    <a href={info[0]} onClick={redirect}>{info[2]}</a>
                </li>
            );
            if (!info[3] || this.state.userinfo.superuser) {
                list.push(item);
            }
        }
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/" onClick={redirect}>图书管理系统</a>
                    </div>
                    <div className="collapse navbar-collapse" id="navbar-collapse">
                        <ul className="nav navbar-nav">
                            {list}
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href={'/user/'+this.state.userinfo.id} onClick={redirect}>{this.state.userinfo.username}</a></li>
                            <li><a href="/logout">登出</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
});
