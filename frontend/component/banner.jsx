'use strict';

var Banner = React.createClass({
    render: function () {
        if (this.props.message) {
            if (this.props.message.type === 'success') {
                return (
                    <div className="alert alert-success">{this.props.message.message}</div>
                );
            } else if (this.props.message.type === 'error') {
                return (
                    <div className="alert alert-danger">{this.props.message.message}</div>
                );
            }
        } else return null;
    }
});

module.exports = React.createClass({
    getInitialState: function () {
        return { message: null };
    },
    componentDidMount: function () {
        eventCenter.on('message', this.message);
        eventCenter.on('clearMessage', this.clearMessage);
    },
    componentWillUnmount: function () {
        eventCenter.removeListener('message', this.message);
        eventCenter.removeListener('clearMessage', this.clearMessage);
    },
    message: function (message) {
        this.setState({ message: message });
    },
    clearMessage: function () {
        this.setState({ message: null });
    },
    render: function () {
        return (<Banner message={this.state.message} />);
    }
});
