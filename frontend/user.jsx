'use strict';

var Table = require('./component/table.jsx');

var titles = [
    '#',
    '用户名',
    '真实姓名',
    '工号',
    '性别',
    '年龄',
    '编辑'
];

module.exports = React.createClass({
    usersUpdated: function (users) {
        this.setState({ users: users });
    },
    getInitialState: function () {
        return { users: [] };
    },
    refresh: function () {
        $.ajax('/api/v1/user', {
            complete: (jqXHR) => {
                this.usersUpdated(jqXHR.responseJSON);
            }
        });
    },
    componentDidMount: function () {
        this.refresh();
    },
    render: function () {
        var body;
        if (!this.state.users) body = [];
        else body = this.state.users.map(item => [
            item.id,
            item.username,
            item.realName,
            item.workNumber,
            item.gender,
            item.age,
            (<div>
                <a href={"/user/"+item.id} onClick={redirect} className="btn">编辑</a>
            </div>)
        ]);
        return (
            <div className="container">
                <input type="button" className="btn" value="创建新用户" onClick={() => redirect('/user/new')} />
                <Table head={titles} body={body} />
            </div>
        );
    }
});
