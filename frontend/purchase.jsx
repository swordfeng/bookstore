'use strict';

var Table = require('./component/table.jsx');

var titles = [
    '#',
    '标题',
    'ISBN',
    '作者',
    '出版社',
    '进货价格',
    '购买数量',
    '进货状态',
    '编辑',
];

var statusNames = {
    'unpaid': '未付款',
    'paid': '已付款',
    'stored': '已库存',
    'canceled': '已退货'
};

module.exports = React.createClass({
    getInitialState: function () {
        return {
            purchases: []
        };
    },
    componentDidMount: function () {
        this.refresh();
    },
    refresh: function () {
        $.ajax('/api/v1/purchase', {
            complete: (xhr) => {
                this.setState({ purchases: xhr.responseJSON });
            }
        });
    },
    pay: function (id) {
        $.ajax('/api/v1/purchase', {
            method: 'POST',
            data: { operation: 'pay', id: id },
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
                this.refresh();
            }
        });
    },
    cancel: function (id) {
        $.ajax('/api/v1/purchase', {
            method: 'POST',
            data: { operation: 'cancel', id: id },
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
                this.refresh();
            }
        });
    },
    store: function (id) {
        $.ajax('/api/v1/purchase', {
            method: 'POST',
            data: { operation: 'store', id: id },
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
                this.refresh();
            }
        });
    },
    render: function () {
        var body = this.state.purchases.map(item => [
            item.id,
            item.book && item.book.title,
            item.book && item.book.isbn,
            item.book && item.book.author,
            item.book && item.book.press,
            item.price,
            item.number,
            statusNames[item.status],
            item.status === 'unpaid' ?
                (<div>
                    <a className="btn" onClick={() => this.pay(item.id)}>付款</a>
                    <a className="btn" onClick={() => this.cancel(item.id)}>退货</a>
                </div>)
            : item.status === 'paid' ?
                (<div>
                    <a className="btn" onClick={() => this.store(item.id)}>到货</a>
                </div>)
            : null
        ]);
        return (
            <div className="container">
                <input type="button" className="btn" value="进货" onClick={() => redirect('/purchase/new')} />
                <Table head={titles} body={body} />
            </div>
        );
    }
});
