'use strict';
var DataForm = require('./component/dataform.jsx');
module.exports = React.createClass({
    getInitialState: function () {
        var schema = {
            id: {
                type: 'text',
                label: 'UserID',
                disabled: true
            },
            username: {
                type: 'text',
                label: 'Username'
            },
            password: {
                type: 'password',
                label: 'Password',
            },
            superuser: {
                type: 'checkbox',
                label: 'Superuser',
                disabled: true
            },
            realName: {
                type: 'text',
                label: '真实姓名'
            },
            workNumber: {
                type: 'text',
                label: '工号'
            },
            gender: {
                type: 'text',
                label: '性别'
            },
            age: {
                type: 'text',
                label: '年龄',
                validate: /^[0-9]*$/
            }
        };
        if (this.props.routeMatch[1]) {
            var id = parseInt(this.props.routeMatch[1]);
            if (!isNaN(id)) {
                schema.password.placeholder = '<unchanged>';
                return { id: id, info: null, schema: schema };
            }
        }
        return { id: null, message: null, schema: schema };
    },
    componentDidMount: function () {
        if (globalState.userinfo) this.setUserInfo(globalState.userinfo);
        eventCenter.on('userinfo', this.setUserInfo);
        if (this.state.id) {
            $.ajax('/api/v1/user?id=' + this.state.id, {
                complete: (xhr) => {
                    var info = xhr.responseJSON;
                    if (info.length > 0) {
                        this.setState({ info: info[0] });
                    }
                }
            });
        }
    },
    componentWillUnmount: function () {
        eventCenter.removeListener('userinfo', this.setUserInfo);
    },
    setUserInfo: function (userinfo) {
        var schema = this.state.schema;
        if (userinfo.superuser) {
            schema.superuser.disabled = false;
            this.setState({ schema: schema })
        }
    },
    submit: function (data) {
        data.id = this.state.id;
        $.ajax('/api/v1/user', {
            method: 'POST',
            data: data,
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
            }
        });
    },
    delete: function (id) {
        $.ajax('/api/v1/user', {
            method: 'DELETE',
            data: { id: id },
            complete: (xhr) => {
                eventCenter.emit('message', xhr.responseJSON);
            }
        });
    },
    render: function () {
        if (!this.state.schema) return null;
        var info;
        if (this.state.id) {
            if (this.state.info === null) return null;
            info = this.state.info;
            for (var idx in info) {
                if (info[idx] === null) info[idx] = '';
            }
        } else {
            info = {
                id: '<new>',
                superuser: false,
            };
        }
        return (
            <div className="container">
                <DataForm onSubmit={this.submit} onDelete={this.delete} schema={this.state.schema} message={this.state.message} data={info} />
            </div>
        );
    }
});
