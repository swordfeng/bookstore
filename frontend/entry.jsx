'use strict';
window.jQuery = window.$ = require('jquery/dist/jquery.min.js');
require('bootstrap/dist/css/bootstrap.min.css');
require('bootstrap/dist/js/bootstrap.min.js');
global.React = require('react');
global.ReactDOM = require('react-dom');

var EventEmitter = require('eventemitter3');
global.eventCenter = new EventEmitter;
global.globalState = {};

window.addEventListener('popstate', () => eventCenter.emit('route'));

global.redirect = function (arg) {
    if (typeof arg.target !== 'undefined') {
        // it's onClick event
        arg.preventDefault();
        if (arg.target.href !== undefined) {
            window.history.pushState({}, '', arg.target.href);
            eventCenter.emit('route');
            if ($('.navbar-collapse.in').length > 0) {
                $('button.navbar-toggle').click();
            }
        }
    } else if (typeof arg === 'string') {
        // arg is url
        window.history.pushState({}, '', arg);
        eventCenter.emit('route');
    }
}

var Router = require('./router.jsx');
var Login = require('./login.jsx');
var Index = require('./index.jsx');
var Users = require('./user.jsx');
var UserEdit = require('./useredit.jsx');
var Book = require('./book.jsx');
var BookEdit = require('./bookedit.jsx');
var Purchase = require('./purchase.jsx');
var PurchasePage = require('./purchasepage.jsx');
var Sale = require('./sale.jsx');
var Financial = require('./financial.jsx');

var routes = [
    [/^\/login$/, Login, '登录'],
    [/^\/$/, Index, '主页'],
    [/^\/user$/, Users, '用户管理'],
    [/^\/user\/(.+)/, UserEdit, '用户信息编辑'],
    [/^\/book$/, Book, '书籍管理'],
    [/^\/book\/(.+)/, BookEdit, '书籍信息编辑'],
    [/^\/purchase$/, Purchase, '进货管理'],
    [/^\/purchase\/(.+)/, PurchasePage, '新书进货'],
    [/^\/sale\/(.+)/, Sale, '书籍销售'],
    [/^\/sale($|\/)/, Sale, '书籍销售'],
    [/^\/financial$/, Financial, '账单管理'],
];

var NotFound = (
    <div>
        Page not found.
    </div>
);

var router = ReactDOM.render(<Router routes={routes} notFound={NotFound} />, document.getElementById('content'));

if (window.location.pathname !== '/login') {
    $.ajax('/api/v1/currentUser', {
        complete: function (jqXHR, textStatus) {
            var userinfo = jqXHR.responseJSON;
            if (jqXHR.status >= 400 || userinfo.type === 'error') {
                // error
                redirect('/login');
            } else {
                globalState.userinfo = userinfo;
                eventCenter.emit('userinfo', userinfo);
            }
        }
    });
}

eventCenter.on('userinfo', userinfo => globalState.userinfo = userinfo);
