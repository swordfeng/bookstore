'use strict';
require('moment').locale(navigator.language);
var DateTimeField = require('react-bootstrap-datetimepicker');
var Table = require('./component/table.jsx');

var titles = [
    '#',
    '类型',
    '数额',
    '描述',
    '创建时间'
];

module.exports = React.createClass({
    getInitialState: function () {
        return { bills: [], from: new Date('Jan 1 2016 GMT+0000'), to: new Date(), dateChanged: false };
    },
    refresh: function (state) {
        $.ajax('/api/v1/bill', {
            data: {
                from: state.from - new Date(0),
                to: state.to - new Date(0)
            },
            success: (bills) => {
                this.setState({ bills: bills });
            }
        });
    },
    componentDidMount: function () {
        this.refresh(this.state);
    },
    setFrom: function (x) {
        if (!parseInt(x)) return;
        this.setState({ dateChanged: true, from: new Date(parseInt(x)) });
    },
    setTo: function (x) {
        if (!parseInt(x)) return;
        this.setState({ dateChanged: true, to: new Date(parseInt(x)) });
    },
    componentWillUpdate: function (nextProps, nextState) {
        if (nextState.dateChanged) {
            this.refresh(nextState);
            this.setState({ dateChanged: false });
        }
    },
    render: function () {
        var body = this.state.bills.map(item => [
            item.id,
            item.type,
            item.value,
            item.description,
            new Date(item.createdAt).toLocaleString()
        ]);
        return (
            <div className="container">
                <div className="col-md-6 col-sm-12 col-xs-12">
                    <label>从</label>
                    <DateTimeField dateTime={this.state.from - new Date(0)} inputFormat="LL LTS" onChange={this.setFrom} />
                </div>
                <div className="col-md-5 col-sm-12 col-xs-12">
                    <label>到</label>
                    <DateTimeField dateTime={this.state.to - new Date(0)} inputFormat="LL LTS" onChange={this.setTo} />
                </div>
                <Table head={titles} body={body} />
            </div>
        );
    }
});
