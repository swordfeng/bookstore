'use strict';

var Table = require('./component/table.jsx');

var titles = [
    '#',
    '标题',
    'ISBN',
    '作者',
    '出版社',
    '单价',
    '库存数量',
    '编辑',
];

module.exports = React.createClass({
    getInitialState: function () {
        return {
            books: [],
            filter: {},
            needRefresh: true,
            refreshing: false
        };
    },
    setFilter: function () {
        this.setState({
            filter: {
                id: parseInt(this.refs.query.id.value) || undefined,
                isbn: this.refs.query.isbn.value,
                title: this.refs.query.title.value,
                author: this.refs.query.author.value,
                press: this.refs.query.press.value,
                store: this.refs.query.store.checked
            },
            needRefresh: true
        });
    },
    refresh: function (filter) {
        if (this.state.refreshing) return;
        this.setState({ refreshing: true, needRefresh: false });
        $.ajax('/api/v1/book', {
            data: filter,
            complete: (jqXHR) => {
                this.setState({
                    refreshing: false,
                    books: jqXHR.responseJSON
                });
            }
        });
    },
    componentDidMount: function () {
        this.refresh({});
    },
    componentWillUpdate: function (newProps, newState) {
        if (newState.needRefresh) {
            this.refresh(newState.filter);
        }
        return true;
    },
    render: function () {
        var body = this.state.books.map(item => [
            item.id,
            item.title,
            item.isbn,
            item.author,
            item.press,
            item.price,
            item.store,
            (<div>
                <a href={"/book/"+item.id} onClick={redirect} className="btn">编辑</a>
                <a href={"/purchase/"+item.id} onClick={redirect} className="btn">进货</a>
                <a href={"/sale/"+item.id} onClick={redirect} className="btn">销售</a>
            </div>)
        ]);
        return (
            <div className="container">
                <input type="button" className="btn" value="添加新书" onClick={() => redirect('/book/new')} />
                <form className="form-inline" ref="query">
                    <div>查询：</div>
                    <div className="form-group">
                        <label for="q-id">#</label>
                        <input type="text" className="form-control" id="q-id" name="id" onChange={this.setFilter} />
                    </div>
                    <div className="form-group">
                        <label for="q-isbn">ISBN</label>
                        <input type="text" className="form-control" id="q-isbn" name="isbn" onChange={this.setFilter} />
                    </div>
                    <div className="form-group">
                        <label for="q-title">标题</label>
                        <input type="text" className="form-control" id="q-title" name="title" onChange={this.setFilter} />
                    </div>
                    <div className="form-group">
                        <label for="q-author">作者</label>
                        <input type="text" className="form-control" id="q-author" name="author" onChange={this.setFilter} />
                    </div>
                    <div className="form-group">
                        <label for="q-press">出版社</label>
                        <input type="text" className="form-control" id="q-press" name="press" onChange={this.setFilter} />
                    </div>
                    <div className="form-group">
                        <label for="q-store">库存</label>
                        <input type="checkbox" className="form-control" id="q-store" name="store" onChange={this.setFilter} />
                    </div>
                </form>
                <Table head={titles} body={body} />
            </div>
        );
    }
});
