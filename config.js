module.exports = {
    db: 'postgres://bookstore:bookstore@localhost/bookstore',
    initUser: 'admin',
    initPassword: 'admin',
    keys: require('localhost.daplie.com-certificates'),
    port: 3000,
    environment: 'production'
};
