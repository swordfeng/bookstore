DB Project - Bookstore Management System
===
数据库期中课程作业：图书管理系统

## 项目结构
```
bookstore/
    package.json            npm项目描述文件
    webpack.config.js       webpack配置文件
    config.js               程序配置文件
    server.js               程序入口
    index.html              基本页面布局
    favicon.ico             网站favicon
    frontend/               浏览器端程序文件夹
        book.jsx            图书列表
        bookedit.jsx        编辑图书信息
        entry.jsx           前端入口
        financial.jsx       账单
        index.jsx           主页
        login.jsx           登录
        purchase.jsx        进货记录
        purchasepage.jsx    进货
        router.jsx          前端路由
        sale.jsx            销售
        user.jsx            用户列表
        useredit.jsx        用户信息编辑
        component/          可复用组件
            banner.jsx      信息提示
            dataform.jsx    简易表单
            isbninput.jsx   ISBN输入框
            isbnscanner.jsx ISBN扫描器
            navbar.jsx      导航条
            table.jsx       表格
    backend/                服务端程序文件夹
        db.js               数据库相关
        db-def.js           数据表定义
        entry.js            后端入口
        login-logout.js     登入/登出/登录检查中间件
        passutil.js         密码操作工具
        rest-api.js         RESTful 接口定义
    doc/                    说明文档
        index.md            主说明
    static/                 webpack 目标目录
```

## URL 定义
```
/                       主页
/login                  用户登录
/user                   用户管理
/book                   书籍管理
/purchase               进货管理
/sale                   图书销售
/financial              财务管理
/api/v1                 RESTFUL API v1 入口
```

## RESTFUL API 定义
`GET /user`  
**urlencoded**  filter  
获取用户信息  

`POST /user`  
**urlencoded** userinfo  
添加或更新用户信息  
当data中有id字段时更新用户信息，否则增加新用户

`GET /book`  
**urlencoded** filter  
获取书籍信息

## 数据库定义（基于 Sequelize 的 ORM）
括号内为自动定义字段
### user 用户信息
```
username    用户名
password    密码(hashed)
salt        密码盐
superuser   超级用户属性
realName    真实姓名
workNumber  工号
gender      性别
age         年龄
(updatedAt) 修改时间
(createdAt) 创建时间
```
### book 图书信息
```
isbn        ISBN 号
title       标题
author      作者
press       出版社
price       单价
store       库存数量
(updatedAt) 修改时间
(createdAt) 创建时间
```
```
### purchase 进货信息
price       单价
number      进货数量
status      进货状态
(bookId)    进货图书
(updatedAt) 修改时间
(createdAt) 创建时间
```
### bill 账单信息
```
type        账类型
value       变化金额
description 说明
(updatedAt) 修改时间
(createdAt) 创建时间
```
