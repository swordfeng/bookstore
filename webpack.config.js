var path = require('path');
var webpack = require('webpack');
module.exports = {
    entry: './frontend/entry.jsx',
    output: {
        path: path.join(__dirname, 'static'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.css$/,   loader: 'style!css' },
            { test: /\.jsx$/,   loader: 'jsx' },
            { test: /\.woff$/,  loader: "url-loader?prefix=static/&mimetype=application/font-woff" },
            { test: /\.woff2$/, loader: "url-loader?prefix=static/&mimetype=application/font-woff2" },
            { test: /\.ttf$/,   loader: "url-loader?prefix=static/&mimetype=application/octet-stream" },
            { test: /\.eot$/,   loader: "url-loader?prefix=static/&mimetype=application/vnd.ms-fontobject" },
            { test: /\.svg$/,   loader: "url-loader?prefix=static/&mimetype=image/svg+xml" }
        ]
    },
    plugins: [
//        new webpack.optimize.DedupePlugin(),
    ]
};
